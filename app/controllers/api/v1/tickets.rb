module API
  module V1
    class Tickets < Grape::API
      include API::V1::Defaults

      resource :tickets do
        desc 'Create a Ticket'
        params do
          requires :user_id, type: Integer, allow_blank: false
          requires :title, type: String, allow_blank: false
          optional :tags, type: Array[String], max_length: 5
        end
        post do
          user_id = permitted_params['user_id']
          title = permitted_params['title']
          tags = permitted_params['tags']

          ticket = TicketService.create_with_tags_and_notify!(
            user_id: user_id,
            title: title,
            tags: tags
          )

          TicketSerializer.new(ticket).serializable_hash.to_json
        end
      end
    end
  end
end
