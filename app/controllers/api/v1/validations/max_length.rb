class MaxLength < Grape::Validations::Validators::Base
  def validate_param!(attr_name, params)
    unless params[attr_name].length <= @option
      raise Grape::Exceptions::Validation.new(
        params: [attr_name.to_s],
        message: "must be at the most #{@option} elements long"
      )
    end
  end
end
