module API
  module V1
    class Base < Grape::API
      require_relative 'validations/max_length'

      mount API::V1::Tickets
    end
  end
end
