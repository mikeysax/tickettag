class Tag < ApplicationRecord
  has_and_belongs_to_many :tickets

  def self.find_or_create_tags_and_associate(ticket, tags = [])
    return if tags.nil? || tags.empty?

    tags.uniq.map do |tag_name|
      tag = find_or_create_by(name: tag_name)
      ticket.tags << tag
      tag
    end
  end
end
