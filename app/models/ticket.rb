class Ticket < ApplicationRecord
  has_and_belongs_to_many :tags

  scope :tag_counts, -> { left_joins(:tags).group(:name).count("tags.name") }

  def self.most_common_tag
    tag_name, tag_count = tag_counts.max_by { |key, value| value }
    { tag_name => tag_count }
  end
end
