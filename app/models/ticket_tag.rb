class TicketTag < ApplicationRecord
  has_many :tickets
  has_many :tags
end
