class TicketService
  def self.create_with_tags_and_notify!(user_id:, title: '', tags: [])
    ticket = nil

    begin
      Ticket.transaction do
        ticket = Ticket.create!(user_id: user_id, title: title)
        Tag.find_or_create_tags_and_associate(ticket, tags) if tags.present?
      end
    rescue StandardError => exception
      Rails.logger.error(exception)
    end

    NotifierService.send(data: Ticket.most_common_tag) if ticket.present?

    ticket
  end
end
