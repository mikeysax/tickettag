class NotifierService
  def self.send(data: {})
    HTTParty.post(
      ENV['NOTIFIER_WEBHOOK_URL'],
      body: JSON.generate(data),
      headers: { 'Content-Type' => 'application/json' }
    )
  end
end
