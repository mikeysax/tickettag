class TicketSerializer
  include JSONAPI::Serializer

  set_type :ticket

  attributes :id,
             :title,
             :created_at,
             :updated_at
end
