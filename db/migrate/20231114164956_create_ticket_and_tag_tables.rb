class CreateTicketAndTagTables < ActiveRecord::Migration[7.0]
  def up
    create_table :tickets do |t|
      t.string :title, null: false
      t.integer :user_id
      t.timestamps
    end

    create_table :tags do |t|
      t.string :name, null: false
      t.timestamps
    end

    create_table :tags_tickets, id: false do |t|
      t.references :ticket, null: false
      t.references :tag, null: false
      t.timestamps
    end

    add_index(:tags, :name, unique: true)
    add_index(:tags_tickets, [:ticket_id, :tag_id], unique: true)
  end

  def down
    drop_table :tags_tickets
    drop_table :tickets
    drop_table :tags
  end
end
