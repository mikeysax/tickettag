require 'rails_helper'

describe API::V1::Tickets, type: :api do
  context 'POST /api/v1/tickets' do
    context 'invalid parameters' do
      it 'throws if user_id is not passed' do
        request = call_api({ title: 'HelloWorld' })
        body = JSON.parse(request.body)

        expect(request.status).to eq(400)
        expect(body['error']).to eq("user_id is missing, user_id is empty")
      end

      it 'throws if user_id is passed but empty' do
        request = call_api({ user_id: nil, title: 'HelloWorld' })
        body = JSON.parse(request.body)

        expect(request.status).to eq(400)
        expect(body['error']).to eq("user_id is empty")
      end

      it 'throws if title is not passed' do
        request = call_api({ user_id: 1 })
        body = JSON.parse(request.body)

        expect(request.status).to eq(400)
        expect(body['error']).to eq("title is missing, title is empty")
      end

      it 'throws if title is passed but empty' do
        request = call_api({ user_id: 1, title: '' })
        body = JSON.parse(request.body)

        expect(request.status).to eq(400)
        expect(body['error']).to eq("title is empty")
      end

      it 'throws if tags exceed 5 values' do
        request = call_api({
          user_id: 1,
          title: 'HelloWorld',
          tags: ['tag1', 'tag2', 'tag3', 'tag4', 'tag5', 'tag6']
        })
        body = JSON.parse(request.body)

        expect(request.status).to eq(400)
        expect(body['error']).to eq("tags must be at the most 5 elements long")
      end
    end

    context 'valid parameters' do
      before do
        stub_request(:post, ENV['NOTIFIER_WEBHOOK_URL']).to_return(:status => 200)
      end

      it 'creates a Ticket successfully' do
        request = call_api({ user_id: 1, title: 'HelloWorld' })
        body = JSON.parse(request.body)

        expect(request.status).to eq(201)
      end

      it 'creates a Ticket without tags' do
        request = call_api({ user_id: 1, title: 'HelloWorld' })
        body = JSON.parse(request.body)
        ticket_id = JSON.parse(body)['data']['id']

        expect(Ticket.find_by(id: ticket_id).present?).to eq(true)
      end

      it 'creates a Ticket with associated Tags deduplicated' do
        request = call_api({ user_id: 1, title: 'HelloWorld', tags: ['tag1', 'tag1'] })
        body = JSON.parse(request.body)
        ticket_id = JSON.parse(body)['data']['id']

        ticket = Ticket.find_by(id: ticket_id)
        tags = ticket.tags

        expect(tags.count).to eq(1)
      end
    end
  end
end
