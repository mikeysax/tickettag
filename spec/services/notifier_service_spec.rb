require 'rails_helper'

describe NotifierService do
  let(:data) do
    { 'tag1' => 3 }
  end

  before do
    stub_request(:post, ENV['NOTIFIER_WEBHOOK_URL']).to_return(:status => 200)
  end

  context 'class methods' do
    context '#send' do
      it 'returns success' do
        response = NotifierService.send(data: data)

        expect(response.success?).to eq(true)
      end
    end
  end
end
