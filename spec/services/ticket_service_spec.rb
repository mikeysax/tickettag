require 'rails_helper'

describe TicketService do
  let(:ticket_title) { 'HelloWorld' }
  let(:user_id) { 1 }

  before do
    stub_request(:post, ENV['NOTIFIER_WEBHOOK_URL']).to_return(:status => 200)
  end

  context 'class methods' do
    context '#create_with_tags_and_notify!' do
      context 'success' do
        it 'creates a Ticket with tags' do
          ticket = TicketService.create_with_tags_and_notify!(
            user_id: user_id,
            title: ticket_title,
            tags: ['tag1', 'tag2']
          )

          expect(ticket.present?).to eq(true)
          expect(ticket.title).to eq(ticket_title)
          expect(ticket.user_id).to eq(user_id)
          expect(ticket.tags.count).to eq(2)
        end

        it 'creates a Ticket without tags' do
          ticket = TicketService.create_with_tags_and_notify!(
            user_id: user_id,
            title: ticket_title,
            tags: []
          )

          expect(ticket.present?).to eq(true)
          expect(ticket.title).to eq(ticket_title)
          expect(ticket.user_id).to eq(user_id)
          expect(ticket.tags.count).to eq(0)
        end
      end

      context 'failure' do
        it 'fails to create a Ticket if transaction fails' do
          ticket = TicketService.create_with_tags_and_notify!(
            user_id: nil,
            title: nil
          )

          expect(ticket.nil?).to eq(true)
        end
      end
    end
  end
end
