require 'rails_helper'

RSpec.describe Tag, type: :model do
  let!(:ticket) { create(:ticket, title: 'ticket1') }
  let!(:tag_name) { 'tag1' }

  context 'class methods' do
    context '#find_or_create_tags_and_associate' do
      context 'failure' do
        it 'returns nil if the tag argument is nil' do
          tags = Tag.find_or_create_tags_and_associate(ticket)

          expect(tags).to eq(nil)
        end

        it 'returns nil if the tag argument is an empty array' do
          tags = Tag.find_or_create_tags_and_associate(ticket, [])

          expect(tags).to eq(nil)
        end
      end

      context 'success' do
        it 'returns created tags' do
          tags = Tag.find_or_create_tags_and_associate(ticket, [tag_name])

          expect(tags.length).to eq(1)
          expect(tags.first.name).to eq(tag_name)
        end

        it 'associates the created tag to the ticket' do
          tags = Tag.find_or_create_tags_and_associate(ticket, [tag_name])

          expect(ticket.tags.count).to eq(1)
          expect(ticket.tags.first.name).to eq(tag_name)
        end
      end
    end
  end
end
