require 'rails_helper'

RSpec.describe Ticket, type: :model do
  let!(:ticket1) { create(:ticket, title: 'ticket1') }
  let!(:ticket2) { create(:ticket, title: 'ticket2') }
  let!(:ticket3) { create(:ticket, title: 'ticket3') }
  let!(:tag1) {
    tag = create(:tag, name: 'tag1')
    ticket1.tags << tag
    ticket2.tags << tag
    ticket3.tags << tag
    tag
  }
  let!(:tag2) {
    tag = create(:tag, name: 'tag2')
    ticket1.tags << tag
    ticket3.tags << tag
    tag
  }
  let!(:tag3) {
    tag = create(:tag, name: 'tag3')
    ticket3.tags << tag
    tag
  }
  let!(:tag4) {
    tag = create(:tag, name: 'tag4')
    ticket1.tags << tag
    ticket2.tags << tag
    tag
  }

  context 'scopes' do
    context 'tag_counts' do
      it 'returns counts for tag frequency' do
        counts = Ticket.tag_counts

        expect(counts.length).to eq(Tag.count)
        expect(counts[tag1.name]).to eq(3)
        expect(counts[tag2.name]).to eq(2)
        expect(counts[tag3.name]).to eq(1)
        expect(counts[tag4.name]).to eq(2)
      end
    end
  end

  context 'class methods' do
    context '#most_common_tag' do
      it 'returns a hash with the most common tag ' do
        most_common_tag = Ticket.most_common_tag

        expect(most_common_tag.length).to eq(1)
        expect(most_common_tag['tag1']).to eq(3)
      end
    end
  end
end
