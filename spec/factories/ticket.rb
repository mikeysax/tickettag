FactoryBot.define do
  factory :ticket do
    user_id { 1 }
    title { 'Hello World' }
  end
end
