# README

### Overview Of Gems Used

This project uses Grape API, JSONApi, Rspec, HTTParty, and various other gems.

### Use

Make post requests to:

```
localhost:3000/api/v1/tickets
```

For creating tickets and associated tags.

Which accepts the follow params:

```
requires :user_id, type: Integer, allow_blank: false
requires :title, type: String, allow_blank: false
optional :tags, type: Array[String], max_length: 5
```

To alter where the webhook response goes for the most frequent tag count, updated the `WEBHOOK_URL` ENV value located in the `.env.local` or `.env.test` for specs.

### Testing

This project uses `rspec` for testing. To trigger specs, use the command `rspec`
